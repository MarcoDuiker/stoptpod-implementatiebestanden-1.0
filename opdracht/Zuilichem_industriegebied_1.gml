<?xml version="1.0" encoding="UTF-8"?>
<geo:GeoInformatieObjectVaststelling xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="https://standaarden.overheid.nl/stop/imop/geo/ ../stop/imop-geo.xsd http://www.geostandaarden.nl/basisgeometrie/1.0 ../xsd/basisgeometrie/basisgeometrie.xsd" xmlns:basisgeo="http://www.geostandaarden.nl/basisgeometrie/1.0" xmlns:data="https://standaarden.overheid.nl/stop/imop/data/" xmlns:geo="https://standaarden.overheid.nl/stop/imop/geo/" xmlns:gio="https://standaarden.overheid.nl/stop/imop/gio/" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:rsc="https://standaarden.overheid.nl/stop/imop/resources/" schemaversie="1.0">
    <geo:context>
        <gio:GeografischeContext>
            <gio:achtergrondVerwijzing>top10nl</gio:achtergrondVerwijzing>
            <gio:achtergrondActualiteit>2019-01-01</gio:achtergrondActualiteit>
        </gio:GeografischeContext>
    </geo:context>
    <geo:vastgesteldeVersie>
        <geo:GeoInformatieObjectVersie>
            <geo:FRBRWork>/join/id/regdata/gm0037/2019/Zuilichem_1</geo:FRBRWork>
            <geo:FRBRExpression>/join/id/regdata/gm0037/2019/Zuilichem_1/nld@2019-06-18;3520</geo:FRBRExpression>
            <geo:eenheidlabel>meter</geo:eenheidlabel>
            <geo:eenheidID>meter</geo:eenheidID>
            <geo:normlabel>maximum bouwhoogte bedrijfsgebouw</geo:normlabel>
            <geo:normID>maximum_bouwhoogte_bedrijfsgebouw</geo:normID>
            <geo:locaties>
                <geo:Locatie>
                    <geo:geometrie>
                        <basisgeo:Geometrie gml:id="id-772B4B37-69F8-4671-B92E-3BCED242409D-xx">
                            <basisgeo:id>772B4B37-69F8-4671-B92E-3BCED242409D</basisgeo:id>
                            <basisgeo:geometrie>
                                <gml:MultiSurface gml:id="id-772B4B37-69F8-4671-B92E-3BCED242409D" srsName="urn:ogc:def:crs:EPSG::28992">
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="id-772B4B37-69F8-4671-B92E-3BCED242409D-1">
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:posList>138488.367 424110.676 138539.221 424103.815 138558.243 424101.248 138677.166 424085.204 138715.527 424371.411 138450.462 424407.085 138413.752 424120.742 138488.367 424110.676</gml:posList>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                </gml:MultiSurface>
                            </basisgeo:geometrie>
                        </basisgeo:Geometrie>
                    </geo:geometrie>
                    <geo:kwantitatieveNormwaarde>7</geo:kwantitatieveNormwaarde>
                </geo:Locatie>
            </geo:locaties>
        </geo:GeoInformatieObjectVersie>
    </geo:vastgesteldeVersie>
</geo:GeoInformatieObjectVaststelling>