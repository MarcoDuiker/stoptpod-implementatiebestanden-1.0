<?xml version="1.0" encoding="UTF-8"?>
<geo:GeoInformatieObjectVaststelling xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="https://standaarden.overheid.nl/stop/imop/geo/ ../stop/imop-geo.xsd http://www.geostandaarden.nl/basisgeometrie/1.0 ../xsd/basisgeometrie/basisgeometrie.xsd" xmlns:basisgeo="http://www.geostandaarden.nl/basisgeometrie/1.0" xmlns:data="https://standaarden.overheid.nl/stop/imop/data/" xmlns:geo="https://standaarden.overheid.nl/stop/imop/geo/" xmlns:gio="https://standaarden.overheid.nl/stop/imop/gio/" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:rsc="https://standaarden.overheid.nl/stop/imop/resources/" schemaversie="1.0">
    <geo:context>
        <gio:GeografischeContext>
            <gio:achtergrondVerwijzing>top10nl</gio:achtergrondVerwijzing>
            <gio:achtergrondActualiteit>2019-01-01</gio:achtergrondActualiteit>
        </gio:GeografischeContext>
    </geo:context>
    <geo:vastgesteldeVersie>
        <geo:GeoInformatieObjectVersie>
            <geo:FRBRWork>/join/id/regdata/gm0037/2019/Zuilichem_2</geo:FRBRWork>
            <geo:FRBRExpression>/join/id/regdata/gm0037/2019/Zuilichem_2/nld@2019-06-18;3520</geo:FRBRExpression>
            <geo:eenheidlabel>meter</geo:eenheidlabel>
            <geo:eenheidID>meter</geo:eenheidID>
            <geo:normlabel>maximum bouwhoogte bedrijfsgebouw</geo:normlabel>
            <geo:normID>maximum_bouwhoogte_bedrijfsgebouw</geo:normID>
            <geo:locaties>
                <geo:Locatie>
                    <geo:geometrie>
                        <basisgeo:Geometrie gml:id="id-D24C3416-4790-461C-A4D0-A82BA60CA7D6">
                            <basisgeo:id>D24C3416-4790-461C-A4D0-A82BA60CA7D6</basisgeo:id>
                            <basisgeo:geometrie>
                                <gml:MultiSurface gml:id="id-D24C3416-4790-461C-A4D0-A82BA60CA7D6-0" srsName="urn:ogc:def:crs:EPSG::28992">
                                    <gml:surfaceMember>
                                        <gml:Polygon gml:id="id-D24C3416-4790-461C-A4D0-A82BA60CA7D6-1">
                                            <gml:exterior>
                                                <gml:LinearRing>
                                                    <gml:posList>138328.581 423893.994 138382.39 424048.862 138340.763 424063.325 138346.082 424078.634 138310.135 424091.12 138310.239 424091.418 138310.466 424092.432 138310.076 424093.396 138309.08 424093.822 138300.082 424096.939 138299.068 424097.196 138298.159 424096.691 138297.694 424095.761 138296.628 424092.698 138298.05 424092.204 138293.702 424079.708 138242.586 424097.436 138188.778 423942.568 138328.581 423893.994</gml:posList>
                                                </gml:LinearRing>
                                            </gml:exterior>
                                        </gml:Polygon>
                                    </gml:surfaceMember>
                                </gml:MultiSurface>
                            </basisgeo:geometrie>
                        </basisgeo:Geometrie>
                    </geo:geometrie>
                    <geo:kwantitatieveNormwaarde>7</geo:kwantitatieveNormwaarde>
                </geo:Locatie>
            </geo:locaties>
        </geo:GeoInformatieObjectVersie>
    </geo:vastgesteldeVersie>
</geo:GeoInformatieObjectVaststelling>