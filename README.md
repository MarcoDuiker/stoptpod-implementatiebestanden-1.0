![](https://landgoed.services/style/img/Landgoed_goud_klein.png) 

STOP/TPOD 1.0 implementatiebestanden
====================================

Aanleiding
----------
De STOP/TPOD standaarden versie 1.0 zijn tezamen met het voorbeeldbestand gemeentestad opgeleverd op de websites van Geonovum en Koop.

De uitlevering is nogal gefragmenteerd en bevat veel fouten. In dit project is gepoogd een werkbare set implementatiebestanden te maken.

Samenstelling
-------------

De volgende onderdelen zijn hier bij elkaar gebracht:

 - het voorbeeldbestand gemeentestad   
   https://geonovum.github.io/TPOD/Voorbeeldbestanden/Vb_Omgevingsplan_Gemeentestad_v1.0.zip
 - OW-schema's   
   https://geonovum.github.io/TPOD/CIMOW/IMOW%20v1.0.zip
 - STOP en LVBB schema's   
   https://gitlab.com/koop/lvbb/bronhouderkoppelvlak/-/tree/master/schema
   
Deze samenstelling is beschikbaar via de Git tag `samenstelling`.

Vervolgens is geprobeerd de problemen in deze samenstelling op te lossen om zo te komen tot een validerend voorbeeldbestand.

Deze poging is helaas nog niet afgerond, en is beschikbaar onder de Git `master` branch.

Validatie
---------

De OW-schema's maken geen gebruik van een XML-catalog. Dat betekent dat veel tools ermee overweg kunnen. Onderstaande problemen en oplossingen zijn gevonden met een bejaarde versie van XMLSpy. Maar dit had ook gekund met tools als Eclipse, Apache Xerces of xmllint.

De LVBB en STOP schema's maken gebruik van een xml catalog volgens de OASIS standaard. Het is nodig dat de gebruikte XML-validator dat ondersteunt. Hier is gebruik gemaakt van Eclipse en xmllint.

Voor Apache Xerces zijn commando's als de volgende gebruikt:

```
StdInParse -v=always -n -s < manifest-ow.xml
```

Voor xmllint zijn commando's als de volgende gebruikt:

```
xmllint --noout --valid --schema ../lvbb/lvbb-stop-aanlevering.xsd akn_nl_bill_gm0037-3520-01.xml
```

Hierbij moet dus steeds het juiste schema worden meegegeven. Het gebruik van de --catalogs en --nocatalogs vlaggen maakte geen verschil voor de resultaten. 


Problemen met oplossingen
-------------------------

#### OW-schema's valideren niet

De schema's van OW valideren niet, doordat er een tweetal verwijzingen vanuit een schema naar een onderliggende schema niet kloppen. Het betreft de verwijzingen naar:
 - IMOW_OWobject.xsd
 - IMOW_GebiedsAanwijzing.xsd

De verwijzingen zijn gerepareerd, waarna de schema's wel valideren.

#### De targetnamespace van de OW voorbeeldbestanden past niet bij de schema's

Alle OW voorbeeldbestanden gebruiken een namespace declaratie met daarin
```
xsi:schemaLocation="http://www.geostandaarden.nl/imow/1.0 ../xsd/bestanden-ow/deelbestand-ow/IMOW_Deelbestand.xsd
```

Helaas leidt dat bij validatie tot foutmeldingen mbt targetnamespace zoals:

```
File opdracht\owActiviteiten-Gemeentestad.xml could not be validated because of an error in XML Schema/DTD (see below) 
Schema at location 'xsd/bestanden­ow/deelbestand-ow/IMOW_Deelbestand.xsd' has target namespace
'http://www.geostandaarden.nl/imow/bestanden/deelbestand' rather than 'http://www.geostandaarden.nl/imow/1.0'.
```

Dit is opgelost door bij de betreffende bestanden een declaratie met
```
xsi:schemaLocation="http://www.geostandaarden.nl/imow/bestanden/deelbestand ../xsd/bestanden-ow/deelbestand-ow/IMOW_Deelbestand.xsd
```
te gebruiken.

#### owPons-Gemeentestad.xml valideert niet

Dit is opgelost door de attributen `WorkIDGIO` en `DeelID` te verwijderen.

#### De verwijzingen naar de schema's kloppen niet in de OP-bestanden

De schema's zijn hier in een ander mapje geplaatst dan waar de voorbeeldbestanden vanuit gaan. Dat is aangepast.

#### Targetnamespace OP bestanden klopt niet

Net als bij de OW-bestanden hebben de OP bestanden last van foutmeldingen bij validatie mbt targetnamespace. Dit is opgelost door in de namespace decalaratie van het voorbeeldbestand op te nemen:

```
xsi:schemaLocation="https://standaarden.overheid.nl/lvbb/stop/aanlevering/ ../lvbb/lvbb-stop-aanlevering.xsd
```



Problemen zonder oplossingen
============================


Validatiefouten OP-schema's
----------------------------

#### lvbb-stop-aanlevering.xsd

Onderstaande foutmeldingen zijn uit xmllint (Eclipse geeft een subset daarvan):

```
../lvbb/lvbb-stop-aanlevering.xsd:58: element element: Schemas parser error : Element '{http://www.w3.org/2001/XMLSchema}element', attribute 'ref': The QName value '{https://standaarden.overheid.nl/stop/imop/data/}ExpressionIdentificatie' does not resolve to a(n) element declaration.
../lvbb/lvbb-stop-aanlevering.xsd:59: element element: Schemas parser error : Element '{http://www.w3.org/2001/XMLSchema}element', attribute 'ref': The QName value '{https://standaarden.overheid.nl/stop/imop/data/}BesluitMetadata' does not resolve to a(n) element declaration.
../lvbb/lvbb-stop-aanlevering.xsd:60: element element: Schemas parser error : Element '{http://www.w3.org/2001/XMLSchema}element', attribute 'ref': The QName value '{https://standaarden.overheid.nl/stop/imop/data/}Procedure' does not resolve to a(n) element declaration.
../lvbb/lvbb-stop-aanlevering.xsd:61: element element: Schemas parser error : Element '{http://www.w3.org/2001/XMLSchema}element', attribute 'ref': The QName value '{https://standaarden.overheid.nl/stop/imop/data/}ConsolidatieInformatie' does not resolve to a(n) element declaration.
../lvbb/lvbb-stop-aanlevering.xsd:63: element element: Schemas parser error : Element '{http://www.w3.org/2001/XMLSchema}element', attribute 'ref': The QName value '{https://standaarden.overheid.nl/stop/imop/tekst/}BesluitKlassiek' does not resolve to a(n) element declaration.
../lvbb/lvbb-stop-aanlevering.xsd:64: element element: Schemas parser error : Element '{http://www.w3.org/2001/XMLSchema}element', attribute 'ref': The QName value '{https://standaarden.overheid.nl/stop/imop/tekst/}BesluitCompact' does not resolve to a(n) element declaration.
../lvbb/lvbb-stop-aanlevering.xsd:78: element element: Schemas parser error : Element '{http://www.w3.org/2001/XMLSchema}element', attribute 'ref': The QName value '{https://standaarden.overheid.nl/stop/imop/tekst/}RegelingKlassiek' does not resolve to a(n) element declaration.
../lvbb/lvbb-stop-aanlevering.xsd:79: element element: Schemas parser error : Element '{http://www.w3.org/2001/XMLSchema}element', attribute 'ref': The QName value '{https://standaarden.overheid.nl/stop/imop/tekst/}RegelingCompact' does not resolve to a(n) element declaration.
../lvbb/lvbb-stop-aanlevering.xsd:80: element element: Schemas parser error : Element '{http://www.w3.org/2001/XMLSchema}element', attribute 'ref': The QName value '{https://standaarden.overheid.nl/stop/imop/tekst/}RegelingTijdelijkdeel' does not resolve to a(n) element declaration.
../lvbb/lvbb-stop-aanlevering.xsd:81: element element: Schemas parser error : Element '{http://www.w3.org/2001/XMLSchema}element', attribute 'ref': The QName value '{https://standaarden.overheid.nl/stop/imop/tekst/}RegelingVrijetekst' does not resolve to a(n) element declaration.
../lvbb/lvbb-stop-aanlevering.xsd:96: element element: Schemas parser error : Element '{http://www.w3.org/2001/XMLSchema}element', attribute 'ref': The QName value '{https://standaarden.overheid.nl/stop/imop/data/}ExpressionIdentificatie' does not resolve to a(n) element declaration.
../lvbb/lvbb-stop-aanlevering.xsd:97: element element: Schemas parser error : Element '{http://www.w3.org/2001/XMLSchema}element', attribute 'ref': The QName value '{https://standaarden.overheid.nl/stop/imop/data/}RegelingMetadata' does not resolve to a(n) element declaration.
../lvbb/lvbb-stop-aanlevering.xsd:105: element element: Schemas parser error : Element '{http://www.w3.org/2001/XMLSchema}element', attribute 'ref': The QName value '{https://standaarden.overheid.nl/stop/imop/data/}ExpressionIdentificatie' does not resolve to a(n) element declaration.
../lvbb/lvbb-stop-aanlevering.xsd:106: element element: Schemas parser error : Element '{http://www.w3.org/2001/XMLSchema}element', attribute 'ref': The QName value '{https://standaarden.overheid.nl/stop/imop/data/}InformatieObjectMetadata' does not resolve to a(n) element declaration.
WXS schema ../lvbb/lvbb-stop-aanlevering.xsd failed to compile
```

#### lvbb-transport.xsd

Uit Eclipse:

```
Description	Resource	Path	Location	Type
src-resolve: Cannot resolve the name 'stop:melding' to a(n) 'type definition' component.	lvbb-transport.xsd	/STOPTPOD10/implementatiebestanden/lvbb	line 396	XML Schema Problem
src-resolve: Cannot resolve the name 'tns:data-reference' to a(n) 'type definition' component.	lvbb-transport.xsd	/STOPTPOD10/implementatiebestanden/lvbb	line 103	XML Schema Problem
```

